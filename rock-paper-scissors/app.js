const playerBox = document.querySelector('.gamebox');
const playersScore = document.querySelector('.players');
const gameTitle = document.querySelector('.gametitle');
const winnerHTML = document.querySelector('.winner');

const dataControl = document.querySelector('.data-control');
const buttons = document.querySelectorAll('.btn');

(function cssInit() {
    playerBox.style.backgroundColor = 'aqua'
    playerBox.style.width = '400px';
    playerBox.style.height = '300px';
    playerBox.style.fontSize = '20px';
    dataControl.style.backgroundColor = 'white'
    dataControl.style.position = 'relative'
    dataControl.style.top = '80px';
    dataControl.style.float = 'bottom'
    for (let button of buttons) {
        button.style.width = '100px';
        button.style.height = '50px';
        button.style.padding = '10px';
        button.style.marginLeft = '20px'
        button.style.color = 'white';
        button.style.background = `rgb(${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)})`;
    }
})();

(function dataInit() {
    gameTitle.innerHTML = 'Rock vs Scissors'
})();

for (let button of buttons) {
    button.addEventListener('click', (e) => btnPressed(e.target.innerText));
}

let playerScore = 0;
let computerScore = 0;
const gameOptions = ['rock', 'paper', 'scissors'];


function btnPressed(option) {
    const playerOption = option.toLowerCase();
    const computerOption = computerChoice();
    gameLogic(playerOption, computerOption);
}

function gameLogic(player, computer) {
    if (player === gameOptions[0]) {
        if (computer === gameOptions[2]) {
            showResults(0);
        }
        else if (computer === gameOptions[1]) {
            showResults(1);
        } else if (computer === gameOptions[0]) {
            showResults(2);
        }
    }
    // ['rock', 'paper', 'scissors'];
    if (player === gameOptions[1]) {
        if (computer === gameOptions[0]) {
            showResults(0);
        } else if (computer === gameOptions[1]) {
            showResults(2);
        } else if (computer === gameOptions[2]) {
            showResults(1);
        }
    }

    if (player === gameOptions[2]) {
        if (computer === gameOptions[0]) {
            showResults[0]
        } else if (computer === gameOptions[1]) {
            showResults[1]
        } else if (computer === gameOptions[2]) {
            showResults[2]
        }
    }
}

function showResults(winner) {
    if (winner === 0) {
        winnerHTML.innerHTML = "Player wins!"
        playerScore++;
    } else if (winner === 1) {
        winnerHTML.innerHTML = "Computer wins!"
        computerScore++;
    } else {
        winnerHTML.innerHTML = 'Draw results in a tie match'
    }

    playersScore.innerHTML = `Player[${playerScore}] : Computer[${computerScore}]`

}

function computerChoice() {
    const choice = gameOptions[Math.floor(Math.random() * 3)];
    return choice;
}
