const button = document.querySelector('button');
const output = document.querySelector('.output');

button.addEventListener('click', showMessage);

// auto-invoking fn
(function initCss() {
    output.style.width = '200px';
    output.style.height = '200px';
    output.style.color = 'white';
    output.style.textAlign = 'center'
})()

function showMessage() {
    const hour = new Date().getHours();
    output.innerHTML = decideMessage(hour);
    output.style.backgroundColor = "black"

}

function decideMessage(hour) {

    let message;

    if (hour > 15) {
        message = "It's evening"
        output.style.color = "red"
    }
    else if (hour > 12) {
        message = "It's afternoon"
        output.style.color = "green"
    }
    else if (hour > 0) {
        message = "It's morning"
        output.style.color = "purple"
    } else {
        message = "You are not on Earth i guess..."
    }
    return `<h1>${message}</h1>`;
}