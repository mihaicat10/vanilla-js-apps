const input = document.querySelector('input');
const button = document.querySelector('button');
const output = document.querySelector('.output');

button.addEventListener('click', () => {
    showMessage();
})

function showMessage() {
    const name = input.value;
    const outputBuilder = `<h5>Hello: ${name}</h5>`;
    output.innerHTML = outputBuilder;
}