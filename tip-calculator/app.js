const button = document.querySelector("button");
const output = document.querySelector(".output");
const cost = document.querySelector("input").value;


function calcTip() {
    const result = `You should tip: ${cost * .15}$ + cost: ${cost}$`
    output.innerHTML = result;
}

button.addEventListener('click', () => {
    calcTip()
})

