const gameBox = document.querySelector('.gamebox');
const computerSelected = document.querySelector('.computer');
const playerSelected = document.querySelector('.player');
const winner = document.querySelector('.winner');
const score = document.querySelector('.score');

const buttons = document.querySelectorAll('button');

(function initCss() {
    gameBox.style.height = '200px';
    gameBox.style.width = '400px';
    gameBox.style.backgroundColor = 'aliceblue';
})()

let playerScore = 0;
let computerScore = 0;

const coins = ["heads", "tails"];

// appending event to buttons
for (let button of buttons) {
    button.addEventListener('click', (e) => calculateWinner(e.target.innerText))
}

function calculateWinner(playerAnswer) {
    const realAnswer = generateAnswer();
    const computerAnswer = generateComputerResponse();

    playerSelected.innerHTML = `Player selected ${computerAnswer.toUpperCase()}`
    computerSelected.innerHTML = `Computer selected ${playerAnswer.toUpperCase()}`

    if (computerAnswer === realAnswer && playerAnswer !== realAnswer) {
        computerScore++;
        winner.innerHTML = `Computer wins!`
    } else if (computerAnswer !== realAnswer && playerAnswer === realAnswer) {
        playerScore++;
        winner.innerHTML = `Player wins!`
    } else {
        winner.innerHTML = `It's a draw!`
    }
    score.innerHTML = `Player ${playerScore} Computer ${computerScore}`
}


function generateComputerResponse() {
    return coins[Math.floor(Math.random() * 2)];
}

function generateAnswer() {
    return coins[Math.floor(Math.random() * 2)];
}

